package au.em.kotlinextensionsvidurshan.extensions

import android.util.Patterns
import java.util.regex.Matcher
import java.util.regex.Pattern

fun String.isValidPhone(): Boolean = this.isNotEmpty() &&
        Patterns.PHONE.matcher(this.trim()).matches() && this.length==10


fun String.isValidEmail(): Boolean {
    var valid: Boolean
    try {
        val pattern: Pattern
        val matcher: Matcher
        val EMAIL_PATTERN =
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"

        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(this)

        valid = matcher.matches()
    } catch (e: Exception) {
        valid = false
        e.printStackTrace()
    }
    return valid
}

fun String.isValidName(): Boolean {
    var valid: Boolean
    try {
        val pattern: Pattern
        val matcher: Matcher
        val PATTERN = "^[A-Za-z-\\s']*+"

        pattern = Pattern.compile(PATTERN)
        matcher = pattern.matcher(this)

        valid = matcher.matches()
    } catch (e: Exception) {
        valid = false
        e.printStackTrace()
    }
    return valid
}

fun String.isValidAddress(): Boolean = this.isNotEmpty()
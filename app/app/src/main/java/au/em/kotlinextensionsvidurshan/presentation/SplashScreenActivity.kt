package au.em.kotlinextensionsvidurshan.presentation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import au.em.kotlinextensionsvidurshan.R

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            var intent = Intent(this,SaveUserDetailsActivity::class.java)
            startActivity(intent)


        }, 2000)
    }
}
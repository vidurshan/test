package au.em.kotlinextensionsvidurshan.presentation

import android.content.SharedPreferences
import android.icu.number.IntegerWidth
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import au.em.kotlinextensionsvidurshan.R
import au.em.kotlinextensionsvidurshan.extensions.*
import au.em.kotlinextensionsvidurshan.model.User
import kotlinx.android.synthetic.main.activity_save_user_details.*

class SaveUserDetailsActivity : AppCompatActivity() {
    lateinit var sharedPreferences: SharedPreferences
    var user: User? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save_user_details)
        editTextEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(editTextEmail.text.toString())
                        .matches()
                ) {
                    correctEmail()
                } else {
                    wrongEmail()
                }
            }
        })

        buttonSave.setOnClickListener(View.OnClickListener {
            if (validData()) {
                user?.fullName = editTextFullName.text.toString()
                user?.phone = editTextPhone.text.toString()
                user?.address = editTextAddress.text.toString()
                user?.gender = getGender()
                user?.email = editTextEmail.text.toString()
                user?.uuid = sharedPreferences.getId().toString()
                saveData()

            }
        })
    }

    fun validData(): Boolean {
        var valid: Boolean = false
        var email: String = editTextEmail.text.toString()
        if (email.isValidEmail()) {
            textViewErrorEmail.visibility = View.GONE
            valid = true
        } else {
            textViewErrorEmail.visibility = View.VISIBLE
            valid = false
        }

        var fullname: String = editTextFullName.text.toString()
        if (fullname.isValidName()) {
            textViewErrorFullName.visibility = View.GONE
            valid = true
        } else {
            textViewErrorFullName.visibility = View.VISIBLE
            valid = false
        }

        var phone: String = editTextPhone.text.toString()
        if (phone.isValidPhone()) {
            textViewErrorPhone.visibility = View.GONE
            valid = true
        } else {
            textViewErrorPhone.visibility = View.VISIBLE
            valid = false
        }
        var address: String = editTextAddress.text.toString()
        if (address.isValidAddress()) {
            textViewErrorAddress.visibility = View.GONE
            valid = true
        } else {
            textViewErrorAddress.visibility = View.VISIBLE
            valid = false
        }



        return valid
    }

    fun correctEmail() {
        textViewErrorEmail.visibility = View.GONE
        editTextEmail.setBackgroundResource(R.drawable.edittext_normal_border)

    }


    fun wrongEmail() {
        textViewErrorEmail.visibility = View.VISIBLE
        editTextEmail.setBackgroundResource(R.drawable.edittext_border_red_error)
    }

    fun getGender(): String {
        val selectedGender: Int = radioGender.checkedRadioButtonId
        if (selectedGender == R.id.radioButtonMale) {
            return "Male"
        } else
            return "Female"
    }


    fun saveData(){
            sharedPreferences.setValue(user!!.uuid,user)
        sharedPreferences.setId((Integer.parseInt(user!!.uuid.toString())+1).toString())
        user =null
        editTextEmail.setText("")
        editTextAddress.setText("")
        editTextFullName.setText("")
        editTextPhone.setText("")
    }

}
package au.em.kotlinextensionsvidurshan.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import au.em.kotlinextensionsvidurshan.R

class DisplayDataActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_data)
    }
}
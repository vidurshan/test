package au.em.kotlinextensionsvidurshan.model

data class User (
    var uuid:String,
    var fullName:String? = null,
    var email:String? = null,
    var phone:String? = null,
    var gender:String? = null,
    var address:String? = null,
       )